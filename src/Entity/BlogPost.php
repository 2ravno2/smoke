<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\BlogPostRepository::class)
 */
class BlogPost
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * // Serializer\Groups(groups={"sonata_api_read", "sonata_api_write", "sonata_search"})
     *
     * @var int
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title='';

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body='';

    /**
     * @var bool
     *
     * @ORM\Column(name="draft", type="boolean")
     */
    private $draft = false;

    /**
     * @var DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @return string
     */
    final public function getTitle():string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    final public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    final public function getBody():string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    final public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    final public function getId():int
    {
        return $this->id??-1;
    }

    /**
     * @return DateTime
     */
    final public function getCreatedAt(): DateTime
    {
        return $this->created??(new DateTime());
    }

    /**
     * @param DateTime $created
     */
    final public function setCreatedAt(DateTime $created): void
    {
        $this->created = $created;
    }

    public function getDraft(): ?bool
    {
        return $this->draft;
    }

    public function setDraft(bool $draft): self
    {
        $this->draft = $draft;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}