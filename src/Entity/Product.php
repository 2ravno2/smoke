<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name='';


    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price=0;


    /**
     * @var bool
     *
     * @ORM\Column(name="popular", type="boolean")
     */
    private $popular=0;

    final public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    final public function getName(): string
    {
        return $this->name??'';
    }

    /**
     * @param string $name
     */
    final public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    final public function getPrice(): int
    {
        return $this->price??0;
    }

    /**
     * @param int $price
     */
    final public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    final public function isPopular(): bool
    {
        return $this->popular??false;
    }

    /**
     * @param bool $popular
     */
    final public function setPopular(bool $popular): void
    {
        $this->popular = $popular;
    }

    final public function getPopular(): ?bool
    {
        return $this->popular;
    }

}
