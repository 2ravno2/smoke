<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Product;
use App\Repository\BlogPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index():Response
    {
        $doc = $this->getDoctrine();
        $posts = $doc->getRepository(BlogPost::class)->findLatest();
        $products = $doc->getRepository(Product::class)->findPopular();
        return $this->render('default/index.html.twig', [
            'popular_products' => $products,
            'latest_posts'     => $posts,
        ]);
    }
}
